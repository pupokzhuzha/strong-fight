package com.Strong.fight.player.row.app

data class StrongInfo(
    val name : String,
    val image : Int,
    val desc : String
)
