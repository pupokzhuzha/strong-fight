package com.Strong.fight.player.row.app

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper

@SuppressLint("CustomSplashScreen")
class HelloScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello_screen)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(MainActivity())
        }, 500)
    }
}