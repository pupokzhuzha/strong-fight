package com.Strong.fight.player.row.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.Strong.fight.player.row.app.databinding.FragmentLobbyScreenBinding

class LobbyScreen : Fragment() {

    private var binding: FragmentLobbyScreenBinding? = null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLobbyScreenBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            best.setOnClickListener {
                findNavController().navigate(R.id.action_lobbyScreen_to_strongScreen)
            }
            exit.setOnClickListener {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}