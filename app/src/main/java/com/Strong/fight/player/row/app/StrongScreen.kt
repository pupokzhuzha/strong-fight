package com.Strong.fight.player.row.app

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.Strong.fight.player.row.app.databinding.FragmentLobbyScreenBinding
import com.Strong.fight.player.row.app.databinding.FragmentStrongScreenBinding

class StrongScreen : Fragment() {

    private var binding: FragmentStrongScreenBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private lateinit var model : StrongInfo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStrongScreenBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = Gen.gen(page)
        mBinding.apply {
            name.text = model.name
            image.setImageResource(model.image)
            desc.text = model.desc


            next.setOnClickListener {
                page++
                model = Gen.gen(page)
                name.text = model.name
                image.setImageResource(model.image)
                desc.text = model.desc

                if(page == 14){
                    shadow.visibility = View.VISIBLE
                    next.isEnabled = false
                }
            }

            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}